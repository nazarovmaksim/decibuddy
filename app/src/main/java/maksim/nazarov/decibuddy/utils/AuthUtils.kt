package maksim.nazarov.decibuddy.utils

import android.accounts.Account
import android.accounts.AccountManager
import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import maksim.nazarov.decibuddy.R

/**
 * Created by Maksim Nazarov on 18.06.2017.
 */
class AuthUtils {

    companion object {

        @SuppressLint("MissingPermission")
        fun createDummyAccount(context: Context): Account? {

            val accountName = "DummyAccount"
            val accountType = context.getString(R.string.account_type)

            // Create the account type and default account
            val newAccount = Account(accountName, accountType)

            // Get an instance of the Android account manager
            val accountManager = context.getSystemService(
                    Context.ACCOUNT_SERVICE) as AccountManager
            /*
         * Add the account and account type, no password or user data
         * If successful, return the Account object, otherwise report an error.
         */
            if (accountManager.addAccountExplicitly(newAccount, null, null)) {
                Log.i("AuthUtils", "Account created!")
            } else {
                /*
             * The account exists or some other error occurred.
             */
                return null
            }

            return newAccount
        }

        fun getAccount(context: Context): Account {
            // Get an instance of the Android account manager
            val accountManager = context.getSystemService(
                    Context.ACCOUNT_SERVICE) as AccountManager

            val accountType = context.getString(R.string.account_type)

            val accounts = accountManager.getAccountsByType(accountType)

            if (accounts.isEmpty()) throw IllegalStateException("There are is no account at all!")

            // return the one and only account
            return accounts[0]
        }
    }
}