package maksim.nazarov.decibuddy.domain.interactors.impl

import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.domain.executor.Executor
import maksim.nazarov.decibuddy.domain.executor.MainThread
import maksim.nazarov.decibuddy.domain.interactors.DeleteChoiceInteractor
import maksim.nazarov.decibuddy.domain.interactors.base.AbstractInteractor
import maksim.nazarov.decibuddy.domain.repository.ChoicesRepository

/**
 * Created by Maksim Nazarov on 14.06.2017.
 */
class DeleteChoiceInteractorImpl(
        threadExecutor: Executor,
        mainThread: MainThread,
        val mRepository: ChoicesRepository,
        val mCallback: DeleteChoiceInteractor.Callback,
        val choice: Choice
): AbstractInteractor(
        threadExecutor,
        mainThread
), DeleteChoiceInteractor {

    override fun run() {
        mRepository.deleteChoice(choice)
        mMainThread.post(Runnable { mCallback.onChoiceDeleted() })
    }
}