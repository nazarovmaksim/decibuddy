package maksim.nazarov.decibuddy.domain.executor

import maksim.nazarov.decibuddy.domain.interactors.base.AbstractInteractor

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
interface Executor {
    fun execute(interactor : AbstractInteractor)
}