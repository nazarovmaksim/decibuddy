package maksim.nazarov.decibuddy.domain.interactors

import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.domain.interactors.base.Interactor

/**
 * Created by Maksim Nazarov on 14.06.2017.
 */
interface DeleteChoiceInteractor : Interactor{
    interface Callback {
        fun onChoiceDeleted()
    }
}