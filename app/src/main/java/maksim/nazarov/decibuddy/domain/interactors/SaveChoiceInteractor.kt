package maksim.nazarov.decibuddy.domain.interactors

import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.domain.interactors.base.Interactor

/**
 * Created by Maksim Nazarov on 16.06.2017.
 */
interface SaveChoiceInteractor: Interactor {

    interface Callback {
        fun onChoicesRetrieved()
    }
}