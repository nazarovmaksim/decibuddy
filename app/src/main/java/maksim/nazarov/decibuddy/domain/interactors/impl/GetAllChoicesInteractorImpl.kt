package maksim.nazarov.decibuddy.domain.interactors.impl

import maksim.nazarov.decibuddy.domain.executor.Executor
import maksim.nazarov.decibuddy.domain.executor.MainThread
import maksim.nazarov.decibuddy.domain.interactors.GetAllChoicesInteractor
import maksim.nazarov.decibuddy.domain.interactors.base.AbstractInteractor
import maksim.nazarov.decibuddy.domain.repository.ChoicesRepository

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
class GetAllChoicesInteractorImpl(
        threadExecutor: Executor,
        mainThread: MainThread,
        val mRepository: ChoicesRepository,
        val mCallback: GetAllChoicesInteractor.Callback
) : AbstractInteractor(
        threadExecutor,
        mainThread
), GetAllChoicesInteractor {

    override fun run() {
        val choices = mRepository.getAllChoices()
        mMainThread.post(Runnable { mCallback.onChoicesRetrieved(choices) })
    }
}