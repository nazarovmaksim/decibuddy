package maksim.nazarov.decibuddy.domain.interactors.impl

import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.domain.executor.Executor
import maksim.nazarov.decibuddy.domain.executor.MainThread
import maksim.nazarov.decibuddy.domain.interactors.SaveChoiceInteractor
import maksim.nazarov.decibuddy.domain.interactors.base.AbstractInteractor
import maksim.nazarov.decibuddy.domain.repository.ChoicesRepository

/**
 * Created by Maksim Nazarov on 16.06.2017.
 */
class SaveChoiceInteractorImpl(
        threadExecutor: Executor,
        mainThread: MainThread,
        val mRepository: ChoicesRepository,
        val choices: List<Choice>
) : AbstractInteractor(
        threadExecutor,
        mainThread
), SaveChoiceInteractor {

    override fun run() {
        mRepository.saveChoices(choices)
    }
}