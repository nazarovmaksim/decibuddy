package maksim.nazarov.decibuddy.domain.executor.impl

import maksim.nazarov.decibuddy.domain.executor.Executor
import maksim.nazarov.decibuddy.domain.interactors.base.AbstractInteractor
import java.util.concurrent.LinkedBlockingQueue
import java.util.concurrent.ThreadPoolExecutor
import java.util.concurrent.TimeUnit

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
class ThreadExecutor private constructor() : Executor {

    private val mThreadPoolExecutor: ThreadPoolExecutor

    companion object{
        @Volatile private var sThreadExecutor : ThreadExecutor? = null

        private val CORE_POOL_SIZE = 3
        private val MAX_POOL_SIZE = 5
        private val KEEP_ALIVE_TIME = 120
        private val TIME_UNIT = TimeUnit.SECONDS
        private val WORK_QUEUE = LinkedBlockingQueue<Runnable>()

        fun getInstance() : Executor{
            if(sThreadExecutor == null) sThreadExecutor = ThreadExecutor()
            return sThreadExecutor as ThreadExecutor
        }
    }

    init {
        mThreadPoolExecutor = ThreadPoolExecutor(CORE_POOL_SIZE, MAX_POOL_SIZE,
                KEEP_ALIVE_TIME.toLong(), TIME_UNIT, WORK_QUEUE)
    }

    override fun execute(interactor: AbstractInteractor) {
        mThreadPoolExecutor.submit({
            interactor.run()
            interactor.onFinished()
        })
    }
}