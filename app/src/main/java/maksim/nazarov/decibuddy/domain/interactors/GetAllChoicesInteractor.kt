package maksim.nazarov.decibuddy.domain.interactors

import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.domain.interactors.base.Interactor

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
interface GetAllChoicesInteractor : Interactor {

    interface Callback {
        fun onChoicesRetrieved(choices: List<Choice>)
    }
}