package maksim.nazarov.decibuddy.domain.interactors.base

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
interface Interactor {
    fun execute()
}