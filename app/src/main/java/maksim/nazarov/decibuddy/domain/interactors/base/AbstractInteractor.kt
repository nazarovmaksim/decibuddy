package maksim.nazarov.decibuddy.domain.interactors.base

import maksim.nazarov.decibuddy.domain.executor.Executor
import maksim.nazarov.decibuddy.domain.executor.MainThread

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
abstract class AbstractInteractor(
        protected val mThreadExecutor: Executor,
        protected val mMainThread: MainThread) {

    @Volatile protected var mIsCanceled: Boolean = false
    @Volatile protected var mIsRunning: Boolean = false

    abstract fun run()

    fun cancel() {
        mIsCanceled = true
        mIsRunning = false
    }

    fun isRunning() = mIsRunning

    fun onFinished() {
        mIsRunning = false
        mIsCanceled = false
    }

    fun execute() {
        mIsRunning = true
        mThreadExecutor.execute(this)
    }
}