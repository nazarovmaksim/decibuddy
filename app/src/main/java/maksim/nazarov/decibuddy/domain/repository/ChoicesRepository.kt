package maksim.nazarov.decibuddy.domain.repository

import maksim.nazarov.decibuddy.data.source.local.model.Choice

/**
 * Created by Maksim Nazarov on 14.06.2017.
 */
interface ChoicesRepository {

    fun getAllChoices() : List<Choice>
    fun saveChoices(listChoices: List<Choice>)
    fun insertNewChoices(choices:List<Choice>)
    fun deleteChoice(choice: Choice)
}