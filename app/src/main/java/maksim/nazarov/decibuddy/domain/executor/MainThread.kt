package maksim.nazarov.decibuddy.domain.executor

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
interface MainThread {
    fun post(runnable: Runnable)
}