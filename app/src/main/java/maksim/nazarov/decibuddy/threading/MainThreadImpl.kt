package maksim.nazarov.decibuddy.threading

import android.os.Handler
import android.os.Looper
import maksim.nazarov.decibuddy.domain.executor.MainThread

/**
 * Created by Maksim Nazarov on 14.06.2017.
 */
class MainThreadImpl private constructor(
        val mHandler: Handler = Handler(Looper.getMainLooper())
) : MainThread {

    companion object {
        private var sMainThread: MainThread? = null

        fun getInstance(): MainThread {
            if (sMainThread == null) sMainThread = MainThreadImpl()
            return sMainThread as MainThread
        }
    }

    override fun post(runnable: Runnable) { mHandler.post(runnable) }
}