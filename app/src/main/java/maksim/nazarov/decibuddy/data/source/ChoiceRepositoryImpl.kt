package maksim.nazarov.decibuddy.data.source

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import maksim.nazarov.decibuddy.data.source.local.database.ChoicesDbHelper
import maksim.nazarov.decibuddy.data.source.local.database.ChoicesPersistenceContract
import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.domain.repository.ChoicesRepository

/**
 * Created by Maksim Nazarov on 14.06.2017.
 */
class ChoiceRepositoryImpl(
        val context : Context
) : ChoicesRepository{

    private val mDbHelper: ChoicesDbHelper = ChoicesDbHelper(context)

    override fun getAllChoices(): List<Choice> {
        val choicesList = ArrayList<Choice>()
        val db = mDbHelper.writableDatabase

        val projection = arrayOf(ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_TIME,
                ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_TEXT,
                ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_COMPLETED)

        val c = db.query(ChoicesPersistenceContract.ChoiceEntry.Companion.TABLE_NAME, projection, null, null, null, null, null)

        if (c != null && c.count > 0) {
            while (c.moveToNext()) {
                choicesList.add(Choice(c.getInt(c.getColumnIndexOrThrow(ChoicesPersistenceContract.ChoiceEntry.COLUMN_NAME_TIME)),
                        c.getString(c.getColumnIndexOrThrow(ChoicesPersistenceContract.ChoiceEntry.COLUMN_NAME_TEXT)),
                        c.getInt(c.getColumnIndexOrThrow(ChoicesPersistenceContract.ChoiceEntry.COLUMN_NAME_COMPLETED))))
            }
            c.close()
        }
        db.close()

        return choicesList
    }

    override fun saveChoices(listChoices: List<Choice>) {
        val db = mDbHelper.writableDatabase
        var query = "INSERT INTO '${ChoicesPersistenceContract.ChoiceEntry.Companion.TABLE_NAME}' ('${ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_TIME}'," +
                "'${ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_TEXT}'," +
                "'${ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_COMPLETED}') VALUES "

        for (i in 0 until listChoices.size-1) {
            query += "('${listChoices[i].mTime}','${listChoices[i].mText}','${listChoices[i].mCompleted}'),"
        }
        query += "('${listChoices[listChoices.size - 1].mTime}','${listChoices[listChoices.size - 1].mText}','${listChoices[listChoices.size - 1].mCompleted}')"
        db.execSQL(query)
    }

    override fun insertNewChoices(choices: List<Choice>) {
        val db = mDbHelper.writableDatabase
        val values = ContentValues()
        for((mTime, mText, mCompleted) in choices){
            values.put(ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_TIME, mTime)
            values.put(ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_TEXT, mText)
            values.put(ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_COMPLETED, mCompleted)
            db.insertWithOnConflict(ChoicesPersistenceContract.ChoiceEntry.Companion.TABLE_NAME, null, values, SQLiteDatabase.CONFLICT_REPLACE)
        }
        db.close()
    }

    override fun deleteChoice(choice: Choice) {
        val db = mDbHelper.writableDatabase
        db.delete(ChoicesPersistenceContract.ChoiceEntry.Companion.TABLE_NAME, "${ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_TIME} = ${choice.mTime}", null)
        db.close()
    }
}