package maksim.nazarov.decibuddy.data.source.local.realm

import io.realm.RealmObject

/**
 * Created by Maksim Nazarov on 22.06.2017.
 */
data class ChoiceRealm(
        var time:Int,
        var text:String,
        var isSelected:Boolean
) : RealmObject()