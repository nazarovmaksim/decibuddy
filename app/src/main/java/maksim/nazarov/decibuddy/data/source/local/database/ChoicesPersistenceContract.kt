package maksim.nazarov.decibuddy.data.source.local.database

object ChoicesPersistenceContract {
    /* Inner class that defines the table contents */
    abstract class ChoiceEntry : android.provider.BaseColumns {
        companion object {
            val TABLE_NAME = "choice"
            val COLUMN_NAME_TIME = "time"
            val COLUMN_NAME_TEXT = "text"
            val COLUMN_NAME_COMPLETED = "completed"
        }
    }
}