package maksim.nazarov.decibuddy.data.source.local.database

class ChoicesDbHelper(context: android.content.Context) : android.database.sqlite.SQLiteOpenHelper(context, maksim.nazarov.decibuddy.data.source.local.database.ChoicesDbHelper.Companion.DATABASE_NAME, null, maksim.nazarov.decibuddy.data.source.local.database.ChoicesDbHelper.Companion.DATABASE_VERSION) {

    companion object {
        private val DATABASE_VERSION = 1
        private val DATABASE_NAME = "Choices.db"

        private val INTEGER_TYPE = " INTEGER"
        private val REAL_TYPE = " REAL"
        private val TEXT_TYPE = " TEXT"
        private val BLOB_TYPE = " BLOB"
        val COMMA_SEP = ","

        val SQL_CREATE_ENTRIES = "CREATE TABLE " + ChoicesPersistenceContract.ChoiceEntry.Companion.TABLE_NAME + " (" +
                ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_TIME + maksim.nazarov.decibuddy.data.source.local.database.ChoicesDbHelper.Companion.INTEGER_TYPE + maksim.nazarov.decibuddy.data.source.local.database.ChoicesDbHelper.Companion.COMMA_SEP +
                ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_TEXT + maksim.nazarov.decibuddy.data.source.local.database.ChoicesDbHelper.Companion.TEXT_TYPE + maksim.nazarov.decibuddy.data.source.local.database.ChoicesDbHelper.Companion.COMMA_SEP +
                ChoicesPersistenceContract.ChoiceEntry.Companion.COLUMN_NAME_COMPLETED + maksim.nazarov.decibuddy.data.source.local.database.ChoicesDbHelper.Companion.INTEGER_TYPE + ")"
    }

    override fun onCreate(db: android.database.sqlite.SQLiteDatabase?) {
        db?.execSQL(maksim.nazarov.decibuddy.data.source.local.database.ChoicesDbHelper.Companion.SQL_CREATE_ENTRIES)
    }

    override fun onUpgrade(db: android.database.sqlite.SQLiteDatabase?, oldVersion: Int, newVersion: Int) {}
}