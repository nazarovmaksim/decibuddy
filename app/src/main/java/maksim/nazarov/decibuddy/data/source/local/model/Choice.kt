package maksim.nazarov.decibuddy.data.source.local.model

/**
 * Created by Maksim Nazarov on 14.06.2017.
 */
data class Choice(val mTime: Int = 0,
                  val mText: String = "",
                  val mCompleted: Int = 0)