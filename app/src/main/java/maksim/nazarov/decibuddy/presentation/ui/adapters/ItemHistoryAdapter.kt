package maksim.nazarov.decibuddy.presentation.ui.adapters

import android.graphics.Color
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import maksim.nazarov.decibuddy.R
import maksim.nazarov.decibuddy.data.source.local.model.Choice

class ItemHistoryAdapter : RecyclerView.Adapter<ItemHistoryAdapter.ItemHistoryViewHolder>() {

    private var mItems: List<Choice>? = null

    fun setItems(items: List<Choice>) {
        mItems = items
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ItemHistoryViewHolder =
            ItemHistoryViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.history_itemlist_item, parent, false))

    override fun getItemCount() = mItems?.size ?: 0

    override fun onBindViewHolder(view: ItemHistoryViewHolder?, position: Int) {
        view?.text?.text = mItems?.get(position)?.mText
        if(mItems?.get(position)?.mCompleted == 0) view?.text?.setBackgroundColor(Color.TRANSPARENT)
        else view?.text?.setBackgroundColor(Color.GREEN)
    }

    class ItemHistoryViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val text: TextView = view.findViewById(R.id.tv_text) as TextView
    }
}