package maksim.nazarov.decibuddy.presentation.presenters.impl

import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.domain.executor.Executor
import maksim.nazarov.decibuddy.domain.executor.MainThread
import maksim.nazarov.decibuddy.domain.interactors.DeleteChoiceInteractor
import maksim.nazarov.decibuddy.domain.interactors.GetAllChoicesInteractor
import maksim.nazarov.decibuddy.domain.interactors.impl.DeleteChoiceInteractorImpl
import maksim.nazarov.decibuddy.domain.interactors.impl.GetAllChoicesInteractorImpl
import maksim.nazarov.decibuddy.domain.repository.ChoicesRepository
import maksim.nazarov.decibuddy.presentation.converter.PresentConverter
import maksim.nazarov.decibuddy.presentation.presenters.AbstractPresenter
import maksim.nazarov.decibuddy.presentation.presenters.HistoryPresenter

/**
 * Created by Nazarov Maksim on 06.06.2017.
 */
class HistoryPresenterImpl(
        executor: Executor,
        mainThread: MainThread,
        val mView: HistoryPresenter.View,
        val mChoicesRepository: ChoicesRepository
) : AbstractPresenter(
        executor,
        mainThread
), HistoryPresenter,
        GetAllChoicesInteractor.Callback,
        DeleteChoiceInteractor.Callback {

    override fun resume() {
        getAllChoices()
    }

    override fun pause() {}

    override fun deleteChoice(choice: Choice) {
        val deleteChoiceInteractor: DeleteChoiceInteractor = DeleteChoiceInteractorImpl(
                mExecuter,
                mMainThread,
                mChoicesRepository,
                this,
                choice
                )
        deleteChoiceInteractor.execute()
    }

    override fun onChoiceDeleted() {
        mView.onChoiceDeleted()
    }

    override fun onChoicesRetrieved(choices: List<Choice>) {
        val values = PresentConverter.groupChoicesByTime(choices)
        mView.showChoices(values)
    }

    override fun getAllChoices() {
        val getAllChoicesInteractor: GetAllChoicesInteractor = GetAllChoicesInteractorImpl(
                mExecuter,
                mMainThread,
                mChoicesRepository,
                this
        )
        getAllChoicesInteractor.execute()
    }
}