package maksim.nazarov.decibuddy.presentation.presenters.impl

import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.domain.executor.Executor
import maksim.nazarov.decibuddy.domain.executor.MainThread
import maksim.nazarov.decibuddy.domain.interactors.SaveChoiceInteractor
import maksim.nazarov.decibuddy.domain.interactors.impl.SaveChoiceInteractorImpl
import maksim.nazarov.decibuddy.domain.repository.ChoicesRepository
import maksim.nazarov.decibuddy.presentation.presenters.AbstractPresenter
import maksim.nazarov.decibuddy.presentation.presenters.ChoicesPresenter
import java.util.*

/**
 * Created by Nazarov Maksim on 06.06.2017.
 */
class ChoicePresenterImpl(
        executor: Executor,
        mainThread: MainThread,
        val mView: ChoicesPresenter.View,
        val mChoicesRepository: ChoicesRepository
) : AbstractPresenter(
        executor,
        mainThread
), ChoicesPresenter {

    override fun resume() {}

    override fun pause() {}

    override fun makeChoice(maxAmount: Int) {
        if (maxAmount < 1) return
        val value = Random().nextInt(maxAmount)
        mView.onClickStart(value)
    }

    override fun addRow() {
        mView.onClickAdd()
    }

    override fun saveChoises(choises: List<Choice>) {
        val saveChoicesInteractor: SaveChoiceInteractor = SaveChoiceInteractorImpl(
                mExecuter,
                mMainThread,
                mChoicesRepository,
                choises
        )
        saveChoicesInteractor.execute()
    }
}