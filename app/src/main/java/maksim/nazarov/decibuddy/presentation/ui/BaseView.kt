package maksim.nazarov.decibuddy.presentation.ui

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
interface BaseView {

    fun showProgress()
    fun hideProgress()
    fun showError()
}