package maksim.nazarov.decibuddy.presentation.converter

import maksim.nazarov.decibuddy.data.source.local.model.Choice

/**
 * Created by Maksim Nazarov on 14.06.2017.
 */
class PresentConverter {
    companion object {

        fun groupChoicesByTime(choices: List<Choice>): List<List<Choice>> {
            if (!choices.isEmpty()) {
                val container = ArrayList<List<Choice>>()
                var time = choices[0].mTime
                var group = ArrayList<Choice>()
                for (choice in choices) {
                    if (time == choice.mTime) {
                        group.add(choice)
                    } else {
                        time = choice.mTime
                        container.add(group)
                        group = ArrayList<Choice>()
                        group.add(choice)
                    }
                }
                container.add(group)
                return container
            } else return emptyList()
        }

        fun createChoicesList(values: List<String>, selectItem: Int): List<Choice> {
            val time = System.currentTimeMillis().toInt()
            val listChoices = arrayListOf<Choice>()
            values.indices.mapTo(listChoices) {
                Choice(mTime = time,
                        mText = values[it],
                        mCompleted = if (it == selectItem) 1 else 0)
            }
            return listChoices
        }
    }
}