package maksim.nazarov.decibuddy.presentation.ui.activities

import android.content.Context
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import kotlinx.android.synthetic.main.choice_activity.*
import maksim.nazarov.decibuddy.R
import maksim.nazarov.decibuddy.data.source.ChoiceRepositoryImpl
import maksim.nazarov.decibuddy.domain.executor.impl.ThreadExecutor
import maksim.nazarov.decibuddy.domain.repository.ChoicesRepository
import maksim.nazarov.decibuddy.presentation.presenters.ChoicesPresenter
import maksim.nazarov.decibuddy.presentation.presenters.impl.ChoicePresenterImpl
import maksim.nazarov.decibuddy.presentation.ui.adapters.ChoiceAdapter
import maksim.nazarov.decibuddy.threading.MainThreadImpl

class ChoiceActivity : AppCompatActivity(), ChoicesPresenter.View {

    private var mPresenter: ChoicesPresenter? = null
    private var mAdapter: ChoiceAdapter? = null

    override fun onCreate(savedInstanceState: android.os.Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.choice_activity)

        interface_setup()
    }

    private fun interface_setup() {
        mPresenter = ChoicePresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                ChoiceRepositoryImpl(this)
        )

        mAdapter = ChoiceAdapter(
                this,
                this
        )

        rv_list.layoutManager = LinearLayoutManager(this)
        rv_list.adapter = mAdapter

        fab_add.setOnClickListener { mPresenter?.addRow() }
        fab_start.setOnClickListener { mPresenter?.makeChoice(mAdapter?.itemCount ?: 0) }
    }

    override fun changeStateAddButton(state: Boolean) {
        if (state) fab_add.visibility = View.VISIBLE
        else fab_add.visibility = View.GONE

        rv_list.smoothScrollToPosition(mAdapter?.itemCount as Int)
    }

    override fun scrollListToEnd() {
        mAdapter?.needScrollToEnd = true
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.resume()
    }

    override fun onPause() {
        super.onPause()
        mPresenter?.pause()
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(maksim.nazarov.decibuddy.R.menu.main_menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == maksim.nazarov.decibuddy.R.id.action_history) startActivityForResult(Intent(this, HistoryActivity::class.java), 123)
        return true
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        data ?: return
        mAdapter?.setItems(data.getStringArrayListExtra("string_list_choices_from_history"))
        mAdapter?.selectItem(data.getIntExtra("number_selected_choice_from_history", -1))
    }

    override fun showProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showError() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onClickAdd() {
        mAdapter?.addItem()
    }

    override fun onClickStart(numberItem: Int) {
        mAdapter?.selectItem(numberItem)
        mPresenter?.saveChoises(mAdapter!!.getChoices())
    }
}