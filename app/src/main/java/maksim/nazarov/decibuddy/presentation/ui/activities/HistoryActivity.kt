package maksim.nazarov.decibuddy.presentation.ui.activities

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.view.MenuItem
import maksim.nazarov.decibuddy.R
import maksim.nazarov.decibuddy.presentation.ui.fragments.HistoryFragment

class HistoryActivity : AppCompatActivity() {


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.history_activity)
        settingSupportActionBar()
        loadHistoryFragment()
    }

    private fun loadHistoryFragment() {
        supportFragmentManager.beginTransaction().add(R.id.fl_content, HistoryFragment()).commit()
    }

    private fun settingSupportActionBar() {
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        if (item?.itemId == android.R.id.home) finish()
        return true
    }
}
