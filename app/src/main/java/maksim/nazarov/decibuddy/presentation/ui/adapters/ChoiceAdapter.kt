package maksim.nazarov.decibuddy.presentation.ui.adapters

import android.content.Context
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.CardView
import android.support.v7.widget.RecyclerView
import android.text.Editable
import android.text.SpannableStringBuilder
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import maksim.nazarov.decibuddy.R
import maksim.nazarov.decibuddy.R.id.*
import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.presentation.converter.PresentConverter.Companion.createChoicesList
import maksim.nazarov.decibuddy.presentation.presenters.ChoicesPresenter
import android.view.inputmethod.InputMethodManager.SHOW_IMPLICIT
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager

class ChoiceAdapter(
        val mContext: Context,
        val mView: ChoicesPresenter.View
) : RecyclerView.Adapter<ChoiceAdapter.MyViewHolder>() {

    private var mItems: ArrayList<String> = arrayListOf("", "")
    private var mSelectedItem: Int = -1

    var needScrollToEnd = false

    fun setItems(items: List<String>) {
        mItems.clear()
        items.map { s -> mItems.add(s) }
        notifyDataSetChanged()
    }

    fun selectItem(item: Int) {
        mSelectedItem = item
        notifyDataSetChanged()
    }

    fun getChoices() = createChoicesList(mItems, mSelectedItem)

    fun addItem() {
        mItems.add("")
        mSelectedItem = -1
        changeAddButton()
        scrollListToEnd()
        notifyDataSetChanged()
    }

    fun removeItemAt(indexItem: Int) {
        mItems.removeAt(indexItem)
        mSelectedItem = -1
        changeAddButton()
        notifyDataSetChanged()
    }

    private fun changeAddButton() = mView.changeStateAddButton(mItems.size in 1..7)

    private fun scrollListToEnd() = mView.scrollListToEnd()

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): MyViewHolder =
            MyViewHolder(LayoutInflater.from(mContext).inflate(R.layout.choice_list_item, parent, false), MyCustomEditTextListener())

    override fun onBindViewHolder(view: MyViewHolder?, position: Int) {
        view?.button?.setOnClickListener { removeItemAt(position) }
        if (mItems.size <= 2) view?.button?.visibility = View.GONE
        else view?.button?.visibility = View.VISIBLE

        view?.myCustomEditTextListener?.updatePosition(position)
        view?.text?.text = SpannableStringBuilder(mItems[position])

        if (position == itemCount - 1 && needScrollToEnd) {
            needScrollToEnd = false
            view!!.text.requestFocus()
            val inputMethodManager = mContext.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            inputMethodManager.showSoftInput(view!!.text, InputMethodManager.SHOW_IMPLICIT)
        }

        if (position == mSelectedItem) view?.container?.setBackgroundColor(android.graphics.Color.GREEN)
        else view?.container?.setBackgroundColor(android.graphics.Color.TRANSPARENT)
    }

    override fun getItemCount(): Int = mItems.size

    inner class MyViewHolder(view: View, var myCustomEditTextListener: MyCustomEditTextListener) : RecyclerView.ViewHolder(view) {
        val text: EditText = view.findViewById(et_text) as EditText
        val button: FloatingActionButton = view.findViewById(fab_add) as FloatingActionButton
        val container: CardView = view.findViewById(cv_container) as CardView

        init {
            text.addTextChangedListener(myCustomEditTextListener)
        }
    }

    inner class MyCustomEditTextListener : TextWatcher {
        private var mPosition: Int? = null

        fun updatePosition(position: Int) {
            mPosition = position
        }

        override fun afterTextChanged(s: Editable?) {}
        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
            mItems[mPosition!!] = s.toString()
        }
    }
}