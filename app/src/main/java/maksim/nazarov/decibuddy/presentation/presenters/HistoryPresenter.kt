package maksim.nazarov.decibuddy.presentation.presenters

import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.presentation.ui.BaseView

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
interface HistoryPresenter : BasePresenter {
    interface View : BaseView{
        fun showChoices(values: List<List<Choice>>)
        fun onChoiceDeleted()

        fun onClickDeleteChoices(choice: Choice)
        fun onClickSelectChoicesFromHistory(choices: List<Choice>)
    }

    fun getAllChoices()
    fun deleteChoice(choice: Choice)
}