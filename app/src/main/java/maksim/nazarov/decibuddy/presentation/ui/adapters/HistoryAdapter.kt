package maksim.nazarov.decibuddy.presentation.ui.adapters

import android.content.Context
import android.support.design.widget.FloatingActionButton
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import maksim.nazarov.decibuddy.R
import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.presentation.presenters.HistoryPresenter

class HistoryAdapter(val mContext: Context,
                     val mView: HistoryPresenter.View) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private var mItems: List<List<Choice>> = emptyList()
    private val map = HashMap<Int, ItemHistoryAdapter>()

    fun setItems(items: List<List<Choice>>) {
        val newList = List(items.size,{ index -> items[items.size-1-index] })
        mItems = newList
        notifyDataSetChanged()
    }

    inner class My2ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val btn_add: FloatingActionButton = view.findViewById(R.id.fab_add) as FloatingActionButton
        val btn_remove: FloatingActionButton = view.findViewById(R.id.fab_remove) as FloatingActionButton
        val rv_list: RecyclerView = view.findViewById(R.id.rv_list) as RecyclerView
    }

    override fun onBindViewHolder(view: RecyclerView.ViewHolder?, position: Int) {
        if (view is My2ViewHolder) {
            if (map[position] == null) map[position] = ItemHistoryAdapter()
            view.rv_list.adapter = map[position]
            view.rv_list.layoutManager = LinearLayoutManager(mContext)
            map[position]?.setItems(mItems[position])

            view.btn_remove.setOnClickListener { mView.onClickDeleteChoices(mItems[position][0]) }
            view.btn_add.setOnClickListener { mView.onClickSelectChoicesFromHistory(mItems[position]) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): My2ViewHolder =
            My2ViewHolder(LayoutInflater.from(parent?.context).inflate(R.layout.history_list_item, parent, false))

    override fun getItemCount(): Int = mItems.size
}