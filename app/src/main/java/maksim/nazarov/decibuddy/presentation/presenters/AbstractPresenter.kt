package maksim.nazarov.decibuddy.presentation.presenters

import maksim.nazarov.decibuddy.domain.executor.Executor
import maksim.nazarov.decibuddy.domain.executor.MainThread

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
abstract class AbstractPresenter(protected val mExecuter : Executor,
                                 protected val mMainThread : MainThread) {}