package maksim.nazarov.decibuddy.presentation.presenters

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
interface BasePresenter {
    fun resume()
    fun pause()
}