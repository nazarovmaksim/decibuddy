package maksim.nazarov.decibuddy.presentation.presenters

import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.presentation.ui.BaseView

/**
 * Created by Maksim Nazarov on 13.06.2017.
 */
interface ChoicesPresenter : BasePresenter {
    interface View : BaseView{
        fun changeStateAddButton(state: Boolean)
        fun scrollListToEnd()

        fun onClickStart(numberItem: Int)
        fun onClickAdd()
    }

    fun makeChoice(maxAmount: Int)
    fun addRow()
    fun saveChoises(choises:List<Choice>)
}