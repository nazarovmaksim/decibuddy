package maksim.nazarov.decibuddy.presentation.ui.fragments

import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import kotlinx.android.synthetic.main.history_fragment.*
import maksim.nazarov.decibuddy.R
import maksim.nazarov.decibuddy.data.source.ChoiceRepositoryImpl
import maksim.nazarov.decibuddy.data.source.local.model.Choice
import maksim.nazarov.decibuddy.domain.executor.impl.ThreadExecutor
import maksim.nazarov.decibuddy.presentation.presenters.HistoryPresenter
import maksim.nazarov.decibuddy.presentation.presenters.impl.HistoryPresenterImpl
import maksim.nazarov.decibuddy.presentation.ui.adapters.HistoryAdapter
import maksim.nazarov.decibuddy.threading.MainThreadImpl

class HistoryFragment : Fragment(), HistoryPresenter.View {

    private var mAdapter: HistoryAdapter? = null
    private var mPresenter: HistoryPresenter? = null

    override fun onCreateView(inflater: LayoutInflater?,
                              container: ViewGroup?,
                              savedInstanceState: Bundle?): View
            = inflater!!.inflate(R.layout.history_fragment, container, false)

    override fun onViewCreated(view: View?, savedInstanceState: Bundle?) {
        rv_list.layoutManager = LinearLayoutManager(context)
        rv_list.adapter = mAdapter
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        init()
    }

    override fun onResume() {
        super.onResume()
        mPresenter?.resume()
    }

    private fun init(){
        mAdapter = HistoryAdapter(context, this)
        mPresenter = HistoryPresenterImpl(
                ThreadExecutor.getInstance(),
                MainThreadImpl.getInstance(),
                this,
                ChoiceRepositoryImpl(context))
    }

    override fun showChoices(values: List<List<Choice>>) {
        mAdapter?.setItems(values)
    }

    override fun onChoiceDeleted() {
        mPresenter?.getAllChoices()
    }

    override fun onClickDeleteChoices(choice: Choice) {
        mPresenter?.deleteChoice(choice)
    }

    override fun onClickSelectChoicesFromHistory(choices: List<Choice>) {
        val intent = Intent()
        val mas = ArrayList<String>()
        choices.mapTo(mas) { it.mText }
        intent.putStringArrayListExtra("string_list_choices_from_history", mas)
        choices.indices
                .filter { choices[it].mCompleted == 1 }
                .forEach { intent.putExtra("number_selected_choice_from_history", it) }
        activity.setResult(123, intent)
        activity.finish()
    }

    override fun showProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun hideProgress() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun showError() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}